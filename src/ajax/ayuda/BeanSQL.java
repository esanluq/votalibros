package ajax.ayuda;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import ajax.bean.Libro;
import ajax.excepcion.MyException;

/**
 * Clase que se encarga de hacer consultas a la Base de Datos
 * 
 * @author Esteban S.L
 *
 */
public class BeanSQL
{
	/**
	 *  Propiedad DataSource que se usará para obtener una conexión a la BD
	 */
	private DataSource DS=null;
	
	/**
	 * Constructor que recibe un DataSource como parámetro
	 * @param DS
	 */
	public BeanSQL(DataSource DS)
	{
		this.DS=DS;
	}
	

	/**
	 * Se encarga de crear un objeto Libro en base a su título
	 * 
	 * @param titulo  Cadena con el título del libro
	 * @return libro  Un Libro
	 * @throws SQLException
	 */
	public Libro getLibro(String titulo) throws SQLException
	{
		Libro libro=new Libro();
		Connection conexion = null;
		Statement st = null;
		ResultSet rs = null;
		String sql;	
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="select * from libros where titulo like '" + titulo + "%'";
		rs = st.executeQuery(sql);
		
		if (rs.next())
		{	
			libro.setTitulo(rs.getString("titulo"));
			libro.setDescripcion(rs.getString("descripcion"));
			libro.setVotospos(rs.getInt("votospos"));
			libro.setVotosneg(rs.getInt("votosneg"));
		}	
		conexion.close();
		return libro;
	}
	
	/**
	 *  Recibe como parámetros el título del libro, y el tipo de voto que se quiere hacer
	 *   y actualiza el registro de votos del libro en cuestión
	 *  
	 * @param titulo	Cadena con el título del libro
	 * @param voto   	Cadena con valores 'votospos' o 'votosneg'
	 * @throws SQLException
	 * @throws MyException 
	 */
	public void updEvalCandidato(String titulo, String voto) 
			throws SQLException, MyException
	{
		Connection conexion = null;
		Statement st = null;
		int inserciones = 0;
		String sql;
		conexion = DS.getConnection();
		st = conexion.createStatement();
		sql="UPDATE libros "
				+ " SET "+voto+"="+ voto +"+ 1"
				+ " WHERE titulo='"+ titulo +"'";
		inserciones = st.executeUpdate(sql);
		if (inserciones==0)
		{
			throw new MyException("No se ha realizado la actualización"
					+ "del libro");
		}
		conexion.close();	
	}
}