package ajax.bean;

/**
 * Clase de tipo POJO para un objeto Libro
 * 
 * @author Esteban
 *
 */
public class Libro
{
	/**
	 *  El título del libro
	 */
	private String titulo;
	/**
	 *  La descripción del libro
	 */
	private String descripcion;
	/**
	 *  Votos positivos asignados por los usuarios
	 */
	private int votospos;
	/**
	 *  Votos negativos de los usuarios del libro
	 */
	private int votosneg;
	
	
	/**
	 *  Constructor sin parámetros
	 */
	public Libro()
	{
		titulo="";
		descripcion="";
		votospos=0;
		votosneg=0;
	}

	/**
	 * @return the titulo
	 */
	public String getTitulo()
	{
		return titulo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion()
	{
		return descripcion;
	}

	/**
	 * @return the votospos
	 */
	public int getVotospos()
	{
		return votospos;
	}

	/**
	 * @return the votosneg
	 */
	public int getVotosneg()
	{
		return votosneg;
	}

	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo)
	{
		this.titulo = titulo;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	/**
	 * @param votospos the votospos to set
	 */
	public void setVotospos(int votospos)
	{
		this.votospos = votospos;
	}

	/**
	 * @param votosneg the votosneg to set
	 */
	public void setVotosneg(int votosneg)
	{
		this.votosneg = votosneg;
	}
}
