package ajax.model;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import ajax.ayuda.BeanSQL;
import ajax.bean.Libro;
import ajax.excepcion.MyException;

/**
 * Clase que se encarga de los procesos propios del modelo Libro
 * 
 * @author Esteban
 * @see http://docs.oracle.com/javaee/1.4/tutorial/doc/JAXPDOM7.html
 */
public class LibroModel
{
	/**
	 *  Propiedad de tipo Libro
	 */
	private Libro libro;
	/**
	 *  Propiedad de tipo Document
	 */
	private Document XmlLibro;
	/**
	 *  Propiedad de tipo DataSource
	 */
	private DataSource ds;
	
	/**
	 *  Constructor que recibe como parámetro un DataSource
	 *  
	 * @param ds Ojeto DataSource
	 */
	public LibroModel(DataSource ds)
	{
		this.ds=ds;
	}
	
	/**
	 * Método que se encarga de crear un documento Xml en base a los datos 
	 * de la propiedad libro
	 * 
	 * @throws ParserConfigurationException
	 */
	private void createXmlDocLibro() throws ParserConfigurationException
	{
		// Se crea un documento XML
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		XmlLibro = builder.newDocument();
		
		// Se crean los nodos padre e hijos con sus correspondientes valores
		Element root = (Element) XmlLibro.createElement("libro");
		
		Element titulo= (Element) XmlLibro.createElement("titulo");
		titulo.appendChild( XmlLibro.createTextNode(libro.getTitulo()));
		
		Element descripcion= (Element) XmlLibro.createElement("descripcion");
		descripcion.appendChild( XmlLibro.createTextNode(libro.getDescripcion()));
		
		Element votospos= (Element) XmlLibro.createElement("votospos");
		votospos.appendChild( XmlLibro.createTextNode(Integer.toString(libro.getVotospos())));
		
		Element votosneg= (Element) XmlLibro.createElement("votosneg");
		votosneg.appendChild( XmlLibro.createTextNode(Integer.toString(libro.getVotosneg())));
		
		// Se añaden los nodos al documento
		XmlLibro.appendChild(root);
		root.appendChild(titulo);
		root.appendChild(descripcion);
		root.appendChild(votospos);
		root.appendChild(votosneg);
	}
	
	/**
	 * Método que se encarga de establecer la respuesta del servidor con los datos de 
	 * un libro en formato XML y volcandolo en forma de flujo al objeto 'response'
	 * 
	 * @param request   Objeto HttpServletRequest
	 * @param response	Objeto HttpServletResponse
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParserConfigurationException
	 * @throws TransformerException
	 */
	public void respuestaLibro(HttpServletRequest request, HttpServletResponse response) 
			throws IOException, SQLException, ParserConfigurationException, TransformerException
	{
		response.setContentType("text/xml;charset=UTF-8");
		PrintWriter out = response.getWriter();
		String titulo= request.getParameter("titulo");
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		transformer = transformerFactory.newTransformer();
		BeanSQL bSQL=new BeanSQL(ds);
		this.libro=bSQL.getLibro(titulo);
		createXmlDocLibro();
		DOMSource source = new DOMSource(XmlLibro);
		 
		StreamResult result = new StreamResult(out);
		transformer.transform(source, result);
	}
	
	/**
	 * Método que se encarga de establecer la respuesta del servidor ante la petición
	 * de voto del cliente
	 * 
	 * @param request Objeto HttpServletRequest
	 * @throws SQLException
	 * @throws MyException
	 */
	public void respuestaVoto(HttpServletRequest request) 
			throws SQLException, MyException
	{
		String titulo=request.getParameter("titulo");
		String voto=request.getParameter("voto");
		BeanSQL bSQL=new BeanSQL(ds);
		
		// El valor de voto es el nombre del campo en la tabla 'libros'
		bSQL.updEvalCandidato(titulo, voto);
	}
}
