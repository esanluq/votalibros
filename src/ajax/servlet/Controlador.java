package ajax.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import ajax.excepcion.MyException;
import ajax.model.LibroModel;

/**
 * Servlet implementation class Controlador
 */
public class Controlador extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
       
	private DataSource ds;
	
	private ServletContext sc;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() 
    {
        super();
    }

    /**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException 
	{
	    super.init(config);
	    try {
	    	sc = config.getServletContext();
	    	Context contexto = new InitialContext();
	    	setDs((DataSource) contexto.lookup(sc.getInitParameter("ds")));
	    	if (getDs()==null)
	    		System.out.println("El DataSource es null.");
	    } 
	    catch(NamingException ne)
	    {
	        // Si no se pudiera recuperar el recurso JNDI asociado al datasource,
	        // se envía un mensaje de error hacia la salida de log del servidor
	        // de aplicaciones.
	        System.out.println("Error: Método init(). tipo NamingException.");
	    } 	    
	}
	
	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() 
	{
	    sc = null;
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		LibroModel modelo=new LibroModel(ds);
		if(request.getParameter("voto")!=null)
		{
			try
			{
				modelo.respuestaVoto(request);
			} 
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (MyException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		else
		{
			try
			{
				modelo.respuestaLibro(request,response);
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			catch (ParserConfigurationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (TransformerException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @param dsBdfotogramas the dsBdfotogramas to set
	 */
	public void setDs(DataSource dsBdfotogramas) {
		this.ds = dsBdfotogramas;
	}

	/**
	 * @return the dsBdfotogramas
	 */
	public DataSource getDs() {
		return ds;
	}
}
