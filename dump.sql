CREATE DATABASE  IF NOT EXISTS `votalibros` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `votalibros`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: votalibros
-- ------------------------------------------------------
-- Server version	5.1.56-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `libros`
--

DROP TABLE IF EXISTS `libros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `libros` (
  `titulo` varchar(200) NOT NULL,
  `descripcion` text NOT NULL,
  `votospos` int(10) unsigned NOT NULL DEFAULT '0',
  `votosneg` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`titulo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libros`
--

LOCK TABLES `libros` WRITE;
/*!40000 ALTER TABLE `libros` DISABLE KEYS */;
INSERT INTO `libros` VALUES ('Código Limpio','Manual de estilo para el desarrollo ágil de software (Programación) de Robert C. Martin ',0,0),('Doctor Zivago','Doctor Zhivago (ruso: Доктор Живаго, Dóktor Zhivago) es una novela de Borís Pasternak, publicada en 1957. Al año siguiente, su autor habría recibido el Premio Nobel de Literatura; sin embargo, lo rechazó. Los acontecimientos se desarrollan con el telón de fondo del frente de batalla de la Primera Guerra Mundial, luego transcurre por la Revolución rusa de 1917, y la posterior Guerra civil de 1918-1920. Gran parte de la trama es absorbida por el idealismo y misticismo del doctor y poeta Zhivago, que en su mente a veces entremezcla fantasía y realidad.',3,0),('Don Quijote de la Mancha','Novela monumental de Miguel de Cervantes y Saavedra, sobre la vida y milagros del Ingenioso Hidalgo Don Quijote, quien perdió la razón del exceso de leer libros de caballería, quien se pensó ser gran caballero desfacedor de entuertos. Al final de su vida, a las puertas de la muerte, volvió en sí, reconociendo su desvarío.',10,0),('El Hobbit','Novela de aventuras, en la que un pequeño y afable habitante de la Comarca se ve envuelto en la épica y peligrosa misión, de arrebatar al dragón Esmaugh, el Reino Bajo la Montaña, que perteneció anteriormente a los Enanos de las colinas de hierro.',4,1),('El Señor de los Anillos','Novela épica de aventuras de R.R. Tolkien. Un grupo de hobbits tienen la misión, casi suicida, de encaminarse hacia el Monte del Destino para poder destruir el anillo único, forjado por Sauron, el señor oscuro, con el fin de dominar la Tierra Media. Libro de obligada lectura para los amantes de las leyendas celtas, y novelas de aventuras en general.',5,1),('La Cabaña de Tio Tom','Novela moralizante. La cabaña del tío Tom (Uncle Tom\'s Cabin) es una novela de la escritora Harriet Beecher Stowe. Se publicó por primera vez el 20 de marzo de 1852.',15,0),('La Sombra del Viento','Novela de intriga, de Carlos Ruiz Zafón. Un amanecer de 1945 un muchacho es conducido por su padre a un misterioso lugar oculto en el corazón de la ciudad vieja: El Cementerio de los Libros Olvidados. Allí, Daniel Sempere encuentra un libro maldito que cambiará el rumbo de su vida y le arrastrará a un laberinto de intrigas y secretos enterrados en el alma oscura de la ciudad. La Sombra del Viento es un misterio literario ambientado en la Barcelona de la primera mitad del siglo XX, desde los últimos esplendores del Modernismo a las tinieblas de la posguerra. La Sombra del Viento mezcla técnicas de relato de intriga, de novela histórica y de comedia de costumbres pero es, sobre todo, una tragedia histórica de amor cuyo eco se proyecta a través del tiempo. Con gran fuerza narrativa, el autor entrelaza tramas y enigmas a modo de muñecas rusas en un inolvidable relato sobre los secretos del corazón y el embrujo de los libros ,manteniendo la intriga hasta la última página.',13,4);
/*!40000 ALTER TABLE `libros` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-03-03  2:17:39
