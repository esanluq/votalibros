/**
 *  Variable global que almacena el objeto XMLHttpRequest
 */
var xmlhttp;

/**
 *  Variable global que almacena el contenido de la caja de texto para 
 *  el título del libro
 */
var titulo;

/**
 *  Esta función únicamente se encarga de crear un objeto XMLHttpRequest
 *  según distintos tipos de navegadores
 */
function creaObjectXmlHttp()
{
	if (window.ActiveXObject) 
	{
		xmlhttp = new ActiveXObject("Microsoft.XMLHttp");
	} 
	else if ((window.XMLHttpRequest) || (typeof XMLHttpRequest) != undefined) 
	{
		xmlhttp = new XMLHttpRequest();
	} 
	else 
	{
		alert("Su navegador no tiene soporte para AJAX");
		return;
	}
}

/**
 * Se encarga de procesar un evento de pulsación de tecla, enviando al servidor 
 * los datos capturados, y procesar la respuesta mediante la función 'enviaTitulo()'
 * @param evento
 */
function busqueda(evento) 
{
	//Comprueba si es un carácter alfanumérico
	if ((evento.keyCode >= 48 && evento.keyCode <= 57) || (evento.keyCode >= 65 && evento.keyCode <= 90)) 
	{
		creaObjectXmlHttp();
		enviaTitulo();
	}
} // fin busqueda(evento)

/**
 *  Se encarga de enviar al servidor un título y llamar a una función que la procesa
 *  usando Ajax 
 */
function enviaTitulo() 
{
	//recupera el texto del título del libro
	titulo = document.getElementById("titulo").value;
	xmlhttp.open("POST", "controlador", true);
	xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xmlhttp.onreadystatechange = procesadatos;
	xmlhttp.send("titulo=" + encodeURIComponent(titulo));
} // fin localizaPalabra()

/**
 *  Se encarga de procesar los datos de la respuesta a la petición de un libro
 *  por su título
 *  
 */
function procesadatos() 
{
	if (xmlhttp.readyState == 4) 
	{
		var respuesta = xmlhttp.responseXML;
		var libro = respuesta.getElementsByTagName("libro").item(0);
		var propiedades = libro.childNodes;
		titulo=propiedades[0].firstChild.nodeValue;
		
		var caja=document.getElementById("titulo");
        if(titulo!="")
        {                    
            var inicioSel=caja.value.length;
            caja.value=titulo;
            caja.selectionStart=inicioSel;
            caja.selectionEnd=caja.value.length;
            document.getElementById("descripcion").innerHTML=propiedades[1].firstChild.nodeValue;
    		document.getElementById("votospos").value=propiedades[2].firstChild.nodeValue;
    		document.getElementById("votosneg").value=propiedades[3].firstChild.nodeValue;
        }
        else
        {
        	titulo="";
        }
	}
} // fin procesadatos()

/**
 * Se encarga de procesar la acción de votar usando Ajax
 * 
 * @param voto  Es el nombre del campo de la tabla 'libros' que se quiere actualizar
 */
function votar(voto)
{
	if(titulo.length>0)
	{
		creaObjectXmlHttp(); 
		xmlhttp.open("POST", "controlador", true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.onreadystatechange = procesaVoto(voto);
		xmlhttp.send("titulo="+titulo+"&voto=" + voto);
	}	
}// fin votar()

/**
 * Se encarga de actualizar las areas de texto
 * con los votos positivos o negativos
 * 
 * @param voto  Es el id del campo que se quiere actualizar
 */
function procesaVoto(voto)
{
	var votosantes=document.getElementById(voto).value;
	document.getElementById(voto).value=parseInt(votosantes)+1;
}// fin procesaVoto(voto)
